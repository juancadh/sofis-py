# coding: utf-8

# In[52]:

import numpy as np
import numpy.polynomial.polynomial as poly


# Explicación de insumos:

# 1. Omega: Matriz de seis por seis. Formato: numpy.ndarray
# Esto corresponde a la matriz de Var-Covar de la página
# Favor ingresarlos con los nombres en el documento de word
# y en el mismo orden. Ver word adjunto:
# [rtml,rtme, et,gildt,gidet,ggpt].

# 1. Tasa de interés nominal de la deuda en moneda local [rtml]
# 2. Tasa de interés nominal de la deuda en moneda extranjera [rtme]
# 3. Tasa de cambio [et]
# 4. Tasa de crecimiento de los ingresos de libre destinación [gildt]
# 5. Tasa de crecimiento de los ingresos con destinación específica [gidet]
# 6. Tasa de crecmiento nominal del gasto primario [ggpt]

# 2. mu_0. Vector de seis dimensiones. Se le debe preguntar al usuario
# en cada una de las seis variables el intercepto.
# en la actualidad no se pregunta en la página. Preguntar como
# Niveles de intercepto del var. Nombres y orden de variables
# comentario anterior aplica. Formato: numpy.ndarray

# 3. mu_1. Matriz de seis por seis. Formato: numpy.ndarray.
# Este valor corresponde a la matriz de "Coeficientes del VAR"
# Comentario de orden y nombres de #1. Omega aplica.

# 4. T. Número de periodos que se va a realizar la predicción. Formato en
# python es: 'int'.

# 5. InitialConditions. Son las condiciones iniciales de las seis variables en el
# periodo a partir del cual se hacen las predicciones. Es un vector de 6x1
# y de formato numpy.ndarray. El orden de las condiciones iniciales es:
# 1. Tasa de interés nominal de la deuda en moneda local [rtml]
# 2. Tasa de interés nominal de la deuda en moneda extranjera [rtme]
# 3. Tasa de cambio [et]
# 4. Tasa de crecimiento de los ingresos de libre destinación [gildt]
# 5. Tasa de crecimiento de los ingresos con destinación específica [gidet]
# 6. Tasa de crecmiento nominal del gasto primario [ggpt]


# 6. ILD0: Nivel de ingresos de libre destinación antes de predicción
# Último dato antes de hacer predicciones. Formato: 'float'

# 7. IDE0: Ingresos de Destinación específica últimos antes de hacer predicción.
# Formato: 'float'

# 8. GP0: Gasto primario antes de hacer predicción. Formato: 'float'.

# 9. e0_level: Nivel inicial de tasa de cambio. Formato: 'float'

# 10. alpha: Porcentaje del a deuda
# en moneda local. Seleccionado por el usuario. Formato: 'float'.

# 11. X: Umbral definido por el usuario en formato 'float'


# 12. D0: Nivel inicial de la deuda.

# Output:

# 1. l1. Serie eje "x" del gráfico de fan charts. Formato numpy.ndarray
# 2. ffit. Línea eje "y" del fan chart. numpy.ndarray
# 3. y1. Serie eje "y" para hacer scatter en fan charts. numpy.ndarray
# 4. PROBA: Probabilidad de que la deuda exceda el umbral. Formato 'numpy.float64'

# Function predicting ILD:
def ILDT(gild, T, ILD0, datapoints=10000):

    # Obtaining ILD: Assuming initial condition for ILD0

    ILD = np.zeros((datapoints, T))

    # Broadcasting initial condition
    ILD[:, 0] = ILD0

    # Predicting
    for t in range(1, T):
        ILD[:, t] = (1 + gild[:, t - 1]) * ILD[:, t - 1]

    return ILD


def FanChartVAR(Omega, mu_0, mu_1, T, InitialConditions, datapoints=10000):

    # Means of shocks is around zero
    means = np.zeros(6)

    # Need to set seed to obtain same shocks every time
    np.random.seed(2581633)

    # Generate vector of (6)X10,000*T
    Y = np.random.multivariate_normal(means, Omega, datapoints * T).T

    # Turn into 3d array
    Y2 = Y.reshape((6, datapoints, T))

    # Generate the vector of predictions
    Predictions = np.zeros((6, datapoints, T))

    # Generate the initial predictions
    Predictions[:, :, 0] = (
        mu_0 + np.matmul(mu_1, InitialConditions) + Y2[:, :, 0].reshape((6, datapoints))
    )

    # Looping over time to obtain the remaining predictions
    for t in range(1, T):
        Predictions[:, :, t] = (
            mu_0
            + np.matmul(mu_1, Predictions[:, :, t - 1])
            + Y2[:, :, t].reshape((6, datapoints))
        )

    return Predictions


# Obtaining the depreciation rate


def Depreciation(et, e0_level, T, datapoints=10000):

    # The e0 is given by the initial condition.
    # et is the prediction of the exchange rates generated in the function FanChartVAR
    DepReturn = np.zeros((datapoints, T))

    # Depreciation between period 0 and 1:
    DepReturn[:, 0] = (et[:, 0] - e0_level) / e0_level
    for t in range(1, T):
        DepReturn[:, t] = (et[:, t] - et[:, t - 1]) / et[:, t - 1]
    return DepReturn


def DeudaTotal(
    Omega,
    mu_0,
    mu_1,
    T,
    InitialConditions,
    D0,
    alpha,
    ILD0,
    IDE0,
    GP0,
    X,
    datapoints=10000,
):

    # First, need to generate all the series of the six variables:
    # 1. Tasa de interés nominal de la deuda en moneda local [rtml]
    # 2. Tasa de interés nominal de la deuda en moneda extranjera [rtme]
    # 3. Tasa de cambio [et]
    # 4. Tasa de crecimiento de los ingresos de libre destinación [gildt]
    # 5. Tasa de crecimiento de los ingresos con destinación específica [gidet]
    # 6. Tasa de crecmiento nominal del gasto primario [ggpt]

    YTOTAL = FanChartVAR(Omega, mu_0, mu_1, T, InitialConditions, datapoints)

    # Load the corresponding output of YTotal into the necessary variables.

    rml = YTOTAL[0, :, :]
    rme = YTOTAL[1, :, :]
    et = YTOTAL[2, :, :]
    gild = YTOTAL[3, :, :]
    gnild = YTOTAL[4, :, :]
    ggp = YTOTAL[5, :, :]

    # 2. Obtain depreciation rate
    DepReturn = np.zeros((datapoints, T))

    e0_level = InitialConditions[2, 0]

    # Depreciation between period 0 and 1:
    DepReturn[:, 0] = (et[:, 0] - e0_level) / e0_level
    for t in range(1, T):
        DepReturn[:, t] = (et[:, t] - et[:, t - 1]) / et[:, t - 1]
    Dep = DepReturn

    # 3. Predicting ILD
    # Obtaining ILD: Assuming initial condition for ILD0

    ILD = np.zeros((datapoints, T))

    # Broadcasting initial condition
    ILD[:, 0] = ILD0
    print("a")
    # Predicting
    for t in range(1, T):
        ILD[:, t] = (1 + gild[:, t - 1]) * ILD[:, t - 1]
    print("b")
    # 4. Obtaining series of ILD, IDE, and GP:
    ILD = ILDT(gild, T, ILD0, datapoints)
    IDE = ILDT(gnild, T, IDE0, datapoints)
    GP = ILDT(ggp, T, GP0, datapoints)

    print("c")
    # 5. FTPRED(ILD,IDE,GP):
    FT = ILD + IDE - GP
    print(FT)
    print("d")

    # 6. Predicting DT:
    DT = np.zeros((datapoints, T))
    DT[:, 0] = D0
    for t in range(1, T):
        DT[:, t] = (
            alpha * (1 + rml[:, t])
            + (1 - alpha) * (1 + rme[:, t]) * (1 + Dep[:, t - 1])
        ) * DT[:, t - 1] - FT[:, t]

    # 7. DT as proportion of ILD:
    FC = DT / ILD

    # 8. Series for fancharts
    l1 = np.zeros(datapoints) + 1
    for d in range(2, T + 1):
        l1 = np.append(l1, np.zeros(datapoints) + d)

    y1 = FC[:, 0]
    for d in range(1, T):
        y1 = np.append(y1, FC[:, d])

    # 9. Fit for fanchart
    coefs = poly.polyfit(l1, y1, 4)
    ffit = poly.polyval(l1, coefs)

    # 10. Probability
    PROBA = sum(FC[:, 4] > X) / datapoints

    # 11. Defining the dictionary:
    Results = dict()

    ## refactor the result

    # y1 => Mat [T, datapoints] = [ [ valores unicos  para T0 ], [ valores unicos  para T1 ], [ valores unicos para T2 ],...]
    # ffit => Mat[T] = [linea media y1 para cada T]
    # ffitsup => Mat[T] = linea sup (valor supior del y1 para ese T)
    # ffitinf => Mat[T] = linea inf (valor inforior del y1 para ese T)

    Results["l1"] = l1.tolist()
    Results["ffit"] = ffit.tolist()
    Results["y1"] = y1.tolist()
    Results["PROBA"] = PROBA

    return Results


# In[53]:

# Testing:
# alpha_TEST = 0.5  ##Proporción de deuda en moneda local
# rt_ml_TEST = 0.01  ##es la tasa de interés nominal de la deuda en moneda local
# rt_me_TEST = 0.02  ##es la tasa de interés nominal de la deuda en moneda extranjera
# deltae_TEST = 0.01  ##Depreciación de la moneda
# Ft_TEST = 0.1  ##Balance primario
# gild_TEST = 1  ##Tasa de crecimiento de ingresos de libre destino
# gide_TEST = 0.01  ##Tasa de crecimiento de ingresos de destinación específica
# ggp_TEST = 0.01  ##Tasa de crecimiento del gasto primario
# GPPrev_TEST = 0.01  ##Gasto primario
# D0_test = 1  ##Nivel inicial de la deuda
# T_test = 5  ##Number of periods that will be predicted
# InitialConditionsTest = (
#     np.random.rand(6, 1) * 0.1
# )  ##This vector stores the level of the following variables in the period
# ##zero: (rt_ml,rt_me,et,gild,tide,ggp)

# ILD0_test = 1
# IDE0_test = 2
# GP0_test = 3
# X_test = 1
# # Psi distribution: Need to assume distribution of six variables. Variance-covariance matrix (OMEGA), mu_0, and mu_1.
# # First, create random matrix:
# np.random.seed(2581633)
# A = np.random.rand(6, 6)

# # Matrix Omega of test. This is the variance covariance matrix of the following variables:
# # (rt_ml,rt_me,et,gild,tide,ggp)

# OMT = np.matmul(A, A.T)

# # Vector mu_0 and matrix mu_1. These parameters describe the evolution of mu_0, mu_1.

# mu_0 = 0.01 * np.arange(6).reshape(6, 1) + 0.1
# mu_1 = np.random.rand(6, 6) * 0.01


# # In[54]:

# DeudaTotal(
#     OMT,
#     mu_0,
#     mu_1,
#     T_test,
#     InitialConditionsTest,
#     D0_test,
#     alpha_TEST,
#     ILD0_test,
#     IDE0_test,
#     GP0_test,
#     X_test,
# )


# In[ ]:

