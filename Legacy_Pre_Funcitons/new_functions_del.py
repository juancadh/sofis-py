    def FanChartPronosticosExternos(T, SeriesExternas, datapoints):

        # Means of shocks is around zero
        means = np.zeros(6)

        # Need to set seed to obtain same shocks every time
        np.random.seed(2581633)

        # con las series externas calcular la varianca(rate_desv_std al cuadrado) de cada dimendon
        Omega = Matridad Identidad *  Vector (varianca(rate_desv_std) al cuadrado)

        # Generate vector of (6)X10,000*T
        Y = np.random.multivariate_normal(means, Omega, datapoints * T).T

        # bound rates to 0 and 1
        Y[0, :] = np.clip(Y[0, :], 0, 1)
        Y[1, :] = np.clip(Y[1, :], 0, 1)

        # Turn into 3d array
        Y2 = Y.reshape((6, datapoints, T))
        print(np.shape(Y2))

        # Generate the vector of predictions
        Predictions = np.zeros((6, datapoints, T))

        # Looping over time to obtain the remaining predictions
        for t in range(0, T):
            Predictions[:, :, t] = SeriesExternas[:, :, t - 1] + Y2[:, :, t].reshape(
                (6, datapoints)
            )

        return Predictions

    def FanChartPronosticosExternosConErrorsCorralacionados(Omega, T, SeriesExternas, datapoints):

        # Means of shocks is around zero
        means = np.zeros(6)

        # Need to set seed to obtain same shocks every time
        np.random.seed(2581633)

        # Generate vector of (6)X10,000*T
        Y = np.random.multivariate_normal(means, Omega, datapoints * T).T

        # bound rates to 0 and 1
        Y[0, :] = np.clip(Y[0, :], 0, 1)
        Y[1, :] = np.clip(Y[1, :], 0, 1)

        # Turn into 3d array
        Y2 = Y.reshape((6, datapoints, T))
        print(np.shape(Y2))

        # Generate the vector of predictions
        Predictions = np.zeros((6, datapoints, T))

        #TODO Initiat prediction in t=0

        # Looping over time to obtain the remaining predictions
        for t in range(0, T):
            Predictions[:, :, t] = SeriesExternas[:, :, t - 1] + Y2[:, :, t].reshape(
                (6, datapoints)
            )

        return Predictions