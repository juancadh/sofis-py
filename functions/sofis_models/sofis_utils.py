import numpy as np
import numpy.polynomial.polynomial as poly


def series_data(a, Tn):
    T = len(a)
    ret = dict()
    ret["mean"] = np.mean(a)
    ret["dif"] = np.subtract(a[1:T], a[0: T - 1])
    ret["rate"] = np.divide(ret["dif"], a[0: T - 1])
    ret["rate_mean"] = np.mean(np.divide(ret["dif"], a[0: T - 1]))
    ret["rate_desv_std"] = np.std(ret["rate"])
    ret["rate_desv_std_pow2"] = np.std(ret["rate"]) ** 2

    if np.any(np.isnan(ret["rate"])) or np.any(np.isinf(ret["rate"])):
        ret["rate"] = np.zeros(len(ret["rate"]))

    if np.any(np.isnan(ret["rate_mean"])) or np.any(np.isinf(ret["rate_mean"])):
        ret["rate_mean"] = 0.0

    if np.any(np.isnan(ret["rate_desv_std"])) or np.any(np.isinf(ret["rate_desv_std"])):
        ret["rate_desv_std"] = 0
        ret["rate_desv_std_pow2"] = 0

    ret["last_rate"] = ret["rate"][-1]

    if Tn - T > 0:
        a_proy = np.zeros(Tn - T)
        a_proy[0] = a[T - 1] * (1 + ret["rate_mean"])

        for t in range(1, Tn - T):
            a_proy[t] = a_proy[t - 1] * (1 + ret["rate_mean"])

        ret["projection"] = np.concatenate([a, a_proy])
    else:
        ret["projection"] = a

    return ret
