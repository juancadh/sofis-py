import numpy as np

# functions
from . import sofis_utils as sofis_utils

# flask
# import sofis_utils as sofis_utils


def aprox(serie, years_to_project, rates):
    if rates == None:
        serie_data = sofis_utils.series_data(serie, len(serie) + years_to_project)
        return serie_data["projection"]
    else:
        projection = np.zeros(years_to_project)
        projection[0] = serie[-1] * (1 + rates[0])
        for i in range(1, years_to_project):
            projection[i] = projection[i - 1] * (1 + rates[i])
        return np.concatenate([serie, projection])
