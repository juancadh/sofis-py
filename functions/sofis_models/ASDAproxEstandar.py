
import numpy as np
import numpy.polynomial.polynomial as poly

# Función para México


def MexicoObtenerSupuestos(tidt, ild, dpt, T):

    # Necesitamos construir tres variables: r, g y d.
    # r-> Promedio de tasa de interés de la deuda la serie. En México es solo una serie.
    # En Otros países es promedio del promedio ponderado de tasa de interés en moneda extranjera y local.
    ild = [float(i) for i in ild]
    tidt = [float(i) for i in tidt]

    r = np.mean(tidt)
    rstd = np.std(tidt)

    # g-> Promedio de tasa de crecimiento de ingresos de libre disposición.
    difG = np.subtract(ild[1:T], ild[0:T-1])
    tasaSerie = np.divide(difG, ild[0:T-1])
    g = np.mean(tasaSerie)
    gstd = np.std(tasaSerie)

    Results = dict()
    Results['r'] = r*100
    Results['rstd'] = rstd
    Results['g'] = g*100
    Results['gstd'] = gstd

    return(Results)


# Función para México
def InternacionalObtenerSupuestos(dtvml, dtifml, dtivme, dtfml, tidme, tidml, T):

    # Necesitamos construir tres variables: r, g y d.
    # r-> En Otros países es promedio del promedio ponderado de tasa de
    # interés en moneda extranjera y local.

    # Obtener promedio ponderado.
    # Primero, deuda total moneda local
    dtml = np.add(dtvml, dtifml)

    # Segundo, deuda total moneda extranjera (en moneda local)
    dtme = np.add(dtivme, dtfml)

    # Tercero, peso de la deuda en moneda local como proporción de la deuda total
    pdlocal = np.divide(dtml, dtml+dtme)

    # Cuarto, promedio ponderado de la tasa de iterés.
    r = np.multiply(tidme, 1-pdlocal)+np.multiply(tidml, pdlocal)

    # Quinto, promedio en los tiempos de la tasa de interes
    rmean = np.mean(r)
    rstd = np.std(r)

    # G. tasa de crecimiento de ingresos de libre disposición. ild

    # g-> Promedio de tasa de crecimiento de ingresos de libre disposición.
    difG = np.subtract(ild[1:T], ild[0:T-1])
    tasaSerie = np.divide(difG, ild[0:T-1])
    g = np.mean(tasaSerie)
    gstd = np.std(tasaSerie)

    Results = dict()
    Results['r'] = rmean*100
    Results['rstd'] = rstd
    Results['g'] = gmean*100
    Results['gstd'] = gstd

    return(Results)


def BalancePrimarioEnPorcentaje(ting, teg, pint, ild):
    return (ting - teg + pint) / ild


def ComputoASD(r, g, rstd, gstd, d, ting, teg, pint, ild, fdesv_g, fdesv_r):

    gs = np.empty(7)
    for i in range(-3, 4):
        gs[i+3] = g + i * gstd * fdesv_g

    rs = np.empty(7)
    for i in range(-3, 4):
        rs[i+3] = r + i * rstd * fdesv_r

    R = np.empty([7, 7])
    R_ajust = np.empty([7, 7])

    balance_primario = BalancePrimarioEnPorcentaje(ting, teg, pint, ild)

    for i in range(0, 7):
        for j in range(0, 7):
            R[j, i] = d * (rs[j] - gs[i]) / (1 + gs[i])

    for i in range(0, 7):
        for j in range(0, 7):
            R_ajust[j, i] = balance_primario - R[j, i]

    Results = dict()

    Results['R'] = R.tolist()
    Results['R_ajust'] = R_ajust.tolist()
    Results['r_header'] = rs.tolist()
    Results['g_header'] = gs.tolist()

    return(Results)
