from enum import Enum
import numpy.polynomial.polynomial as poly
import numpy as np


# functions
from . import sofis_utils as sofis_utils

# flask
# import sofis_utils as sofis_utils


class FanChartModels(Enum):
    VAR = "var"
    External = "external"
    External_w_Errors = "externalwithcorrelatederrors"
    Weighted = "weighted"


# Function predicting ILD:
def ILDT(gild, T, ILD0, datapoints):

    # Obtaining ILD: Assuming initial condition for ILD0

    ILD = np.zeros((datapoints, T))

    # Broadcasting initial condition
    ILD[:, 0] = ILD0

    # Predicting
    for t in range(1, T):
        ILD[:, t] = (1 + gild[:, t - 1]) * ILD[:, t - 1]

    return ILD


def FanChartVAR(Omega, mu_0, mu_1, T, InitialConditions, datapoints):
    # Means of shocks is around zero
    means = np.zeros(6)

    # Need to set seed to obtain same shocks every time
    np.random.seed(2581633)

    # Generate vector of (6)X10,000*T
    Y = np.random.multivariate_normal(means, Omega, datapoints * T).T

    # bound rates to 0 and 1
    Y[0, :] = np.clip(Y[0, :], 0, 1)
    Y[1, :] = np.clip(Y[1, :], 0, 1)

    # Turn into 3d array
    Y2 = Y.reshape((6, datapoints, T))
    # Generate the vector of predictions
    Predictions = np.zeros((6, datapoints, T))

    # Generate the initial predictions
    # Y_(T+i)=μ_o+μ_1 Y_(t+i-1)+ξ_(t+i)
    Predictions[:, :, 0] = (
        mu_0 + np.matmul(mu_1, InitialConditions) +
        Y2[:, :, 0].reshape((6, datapoints))
    )

    # Looping over time to obtain the remaining predictions
    for t in range(1, T):
        Predictions[:, :, t] = (
            mu_0
            + np.matmul(mu_1, Predictions[:, :, t - 1])
            + Y2[:, :, t].reshape((6, datapoints))
        )

    return Predictions


def FanChartPronosticosExternos(T, Omega, SeriesExternas, datapoints):

    # Means of shocks is around zero
    means = np.zeros(6)

    # Need to set seed to obtain same shocks every time
    np.random.seed(2581633)

    # con las series externas calcular la varianca(rate_desv_std al cuadrado) de cada dimendon
    # Generate vector of (6)X10,000*T
    Y = np.random.multivariate_normal(means, Omega, datapoints * T).T
    # bound rates to 0 and 1
    Y[0, :] = np.clip(Y[0, :], 0, 1)
    Y[1, :] = np.clip(Y[1, :], 0, 1)

    # Turn into 3d array
    Y2 = Y.reshape((6, datapoints, T))
    # Generate the vector of predictions
    Predictions = np.zeros((6, datapoints, T))

    RepeatSeriesExternas = np.repeat(
        SeriesExternas[:, :].reshape((6 * T, 1)), datapoints
    ).reshape((6, datapoints, T))

    # Looping over time to obtain the remaining predictions
    for t in range(0, T):
        Predictions[:, :, t] = RepeatSeriesExternas[:, :, t] + Y2[:, :, t]

    return Predictions


# Obtaining the depreciation rate
def Depreciation(et, e0_level, T, datapoints):

    # The e0 is given by the initial condition.
    # et is the prediction of the exchange rates generated in the function FanChartVAR
    DepReturn = np.zeros((datapoints, T))

    # Depreciation between period 0 and 1:
    DepReturn[:, 0] = (et[:, 0] - e0_level) / e0_level
    for t in range(1, T):
        DepReturn[:, t] = (et[:, t] - et[:, t - 1]) / et[:, t - 1]
    return DepReturn


def CalculateResults(
    YTOTAL,
    T,
    datapoints,
    DebtInitialValue,
    ILDT,
    ILD0,
    IDE0,
    ggpt,
    Gp0,
    X,
    alpha,
    per_sup,
    per_inf,
):
    rml = YTOTAL[0, :, :]
    rme = YTOTAL[1, :, :]
    et = YTOTAL[2, :, :]
    gild = YTOTAL[3, :, :]
    gnild = YTOTAL[4, :, :]
    ggp = YTOTAL[5, :, :]

    # 2. Obtain depreciation rate
    DepReturn = np.zeros((datapoints, T))

    e0_level = et[:, -1]

    # Depreciation between period 0 and 1:
    DepReturn[:, 0] = (et[:, 0] - e0_level) / e0_level

    for t in range(1, T):
        DepReturn[:, t] = (et[:, t] - et[:, t - 1]) / et[:, t - 1]
    Dep = DepReturn

    # 3. Predicting ILD
    # Obtaining ILD: Assuming initial condition for ILD0

    ILD = np.zeros((datapoints, T))

    # Broadcasting initial condition
    ILD[:, 0] = ILD0
    # Predicting
    for t in range(1, T):
        ILD[:, t] = (1 + gild[:, t - 1]) * ILD[:, t - 1]

    # 4. Obtaining series of ILD, IDE, and GP:
    ILD = ILDT(gild, T, ILD0, datapoints)
    IDE = ILDT(gnild, T, IDE0, datapoints)
    GP = ILDT(ggp, T, Gp0, datapoints)

    # 5. FTPRED(ILD,IDE,GP):
    FT = ILD + IDE - GP

    # 6. Predicting DT:
    DT = np.zeros((datapoints, T))
    DT[:, 0] = DebtInitialValue
    for t in range(1, T):
        DT[:, t] = (
            alpha * (1 + rml[:, t]) + (1 - alpha) *
            (1 + rme[:, t]) * (1 + et[:, t])
        ) * DT[:, t - 1] - FT[:, t]

    # 7. DT as proportion of ILD:
    FC = DT / ILD

    # 8. Series for fancharts
    l1 = np.zeros(datapoints) + 1
    for d in range(2, T + 1):
        l1 = np.append(l1, np.zeros(datapoints) + d)

    y1 = FC[:, 0]
    for d in range(1, T):
        y1 = np.append(y1, FC[:, d])

    # 9. Fit for fanchart
    coefs = poly.polyfit(l1, y1, 4)
    ffit = poly.polyval(l1, coefs)

    # 10. Probability
    # Value at Risk y análisis de sensibilidad donde X es el umbral del entrado por el usuario
    PROBA = sum(FC[:, 4] > X) / datapoints

    # 11. Defining the dictionary:
    Results = dict()

    y1 = y1.reshape(T, datapoints)

    # Only keeping the corresponding values for ffit not to run everything
    ffit = ffit.reshape(T, datapoints)
    ffit = ffit[0:T, 0]

    # Maximum
    ffitsup = np.percentile(y1, per_sup, 1, interpolation="nearest")
    ffitinf = np.percentile(y1, per_inf, 1, interpolation="nearest")

    series_perentiles = []
    for i in range(0, 40, 10):
        serie = {}
        serie["p_inf"] = per_inf + i
        serie["p_sup"] = per_sup - i
        serie["inf"] = np.percentile(
            y1, per_inf + i, 1, interpolation="nearest"
        ).tolist()
        serie["sup"] = np.percentile(
            y1, per_sup - i, 1, interpolation="nearest"
        ).tolist()
        series_perentiles.append(serie)

    # Results["l1"] = l1.tolist()
    Results["ffit"] = ffit.tolist()
    Results["series_perentiles"] = series_perentiles
    # Results["y1"] = y1.tolist()
    Results["ffitsup"] = ffitsup.tolist()
    Results["ffitinf"] = ffitinf.tolist()
    Results["PROBA"] = PROBA

    # Mock for Analisis de sensibilidad
    Results["sensitivity_analysis"] = [
        [1, 2, 3, 4],
        [1, 2, 3, 4],
        [1, 2, 3, 4],
        [1, 2, 3, 4],
        [1, 2, 3, 4],
    ]
    return Results


def DeudaTotal(
    Omega,
    mu_0,
    mu_1,
    T,
    Rtml,
    Rtme,
    gildt,
    gidet,
    ggpt,
    et,
    SeriesExternas,
    DebtInitialValue,
    Gp0,
    ILD0,
    IDE0,
    alpha,
    beta,
    X,
    datapoints,
    per_sup,
    per_inf,
    model=FanChartModels.VAR,
):

    print("Fanchart Model " + model)
    # Calcular las tasasa de crecimiento hasta el último periodo

    # Length of the dataset used: Rtml
    calculosRtml = sofis_utils.series_data(Rtml, T)
    calculosRtme = sofis_utils.series_data(Rtme, T)
    calculosgildt = sofis_utils.series_data(gildt, T)
    calculosgidet = sofis_utils.series_data(gidet, T)
    calculosggpt = sofis_utils.series_data(ggpt, T)
    calculoset = sofis_utils.series_data(et, T)

    InitialConditions = np.zeros((6, 1))

    InitialConditions[0] = calculosRtml["last_rate"]
    InitialConditions[1] = calculosRtme["last_rate"]
    InitialConditions[2] = calculoset["last_rate"]
    InitialConditions[3] = calculosgildt["last_rate"]
    InitialConditions[4] = calculosgidet["last_rate"]
    InitialConditions[5] = calculosggpt["last_rate"]

    # TODO FanChartVAR
    YTOTAL_VAR = FanChartVAR(Omega, mu_0, mu_1, T,
                             InitialConditions, datapoints)
    if model != FanChartModels.VAR.value:
        rate_desv_pow2 = np.zeros((6, 1))
        rate_desv_pow2[0] = calculosRtml["rate_desv_std_pow2"]
        rate_desv_pow2[1] = calculosRtme["rate_desv_std_pow2"]
        rate_desv_pow2[2] = calculoset["rate_desv_std_pow2"]
        rate_desv_pow2[3] = calculosgildt["rate_desv_std_pow2"]
        rate_desv_pow2[4] = calculosgidet["rate_desv_std_pow2"]
        rate_desv_pow2[5] = calculosggpt["rate_desv_std_pow2"]

        Omega_no_correlacionada = np.identity(6) * rate_desv_pow2

        if (
            model == FanChartModels.External.value
            or model == FanChartModels.Weighted.value
        ):
            YTOTAL_EXT = FanChartPronosticosExternos(
                T, Omega_no_correlacionada, SeriesExternas, datapoints
            )
        else:
            YTOTAL_EXT = FanChartPronosticosExternos(
                T, Omega, SeriesExternas, datapoints
            )

    YTOTAL = YTOTAL_VAR

    if model == FanChartModels.Weighted.value:
        YTOTAL = beta * YTOTAL_VAR + (1.0 - beta) * YTOTAL_EXT

    if (
        model == FanChartModels.External_w_Errors.value
        or model == FanChartModels.External.value
    ):
        YTOTAL = YTOTAL_EXT

    return CalculateResults(
        YTOTAL,
        T,
        datapoints,
        DebtInitialValue,
        ILDT,
        ILD0,
        IDE0,
        ggpt,
        Gp0,
        X,
        alpha,
        per_sup,
        per_inf,
    )
