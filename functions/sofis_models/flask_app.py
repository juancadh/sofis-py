import FanChartsFunction
import ASDAproxEstandar
import ASDDinamicaDeuda
import mfmp
from flask import Flask, jsonify, abort, request
from flask_cors import CORS
import numpy as np

app = Flask(__name__)
CORS(app)


@app.route("/api/v1.0/", methods=["GET"])
def info():
    return "SOFIS API - version 1.0"


@app.route("/api/v1.0/mfmp", methods=["POST"])
def mfmp_api():
    rates = request.json["rates"]
    years_to_project = request.json["years_to_project"]
    serie = request.json["serie"]

    results = mfmp.aprox(serie, years_to_project, rates)

    return jsonify(results.tolist()), 201


# ASD Fanchart
@app.route("/api/v1.0/ASD/totaldebt/", methods=["POST"])
def totaldebt():
    T = request.json["T"]
    X = request.json["X"]
    alpha = request.json["alpha"]
    IDE0 = request.json["IDE0"]
    ILD0 = request.json["ILD0"]
    GP0 = request.json["GP0"]
    D0 = request.json["D0"]
    datapoints = request.json["datapoints"]

    rtml = request.json["rtml"]
    rtme = request.json["rtme"]
    et = request.json["et"]
    gildt = request.json["gildt"]
    gidet = request.json["gidet"]
    ggpt = request.json["ggpt"]

    OMT = params_to_mat(request.json["OMT"])
    mu_0 = params_to_mat(request.json["mu_0"])
    mu_1 = params_to_mat(request.json["mu_1"])

    per_sup = 95
    per_inf = 5

    if "per_sup" in request.json:
        per_sup = request.json["per_sup"]
    if "per_inf" in request.json:
        per_inf = request.json["per_inf"]

    totaldebt = FanChartsFunction.DeudaTotal(
        OMT,
        mu_0,
        mu_1,
        T,
        rtml,
        rtme,
        gildt,
        gidet,
        ggpt,
        et,
        D0,
        GP0,
        ILD0,
        IDE0,
        alpha,
        X,
        datapoints,
        per_sup,
        per_inf,
    )

    return jsonify({"totaldebt": totaldebt}), 201


# ASD Approximacion Standard
@app.route("/api/v1.0/ASD/standarapprox/inputs", methods=["POST"])
def asd_approxstandard_inputs():

    catalog = request.json["catalog"]
    T = request.json["T"]

    if catalog == "M":
        ild = request.json["ild"]
        tidt = request.json["tidt"]
        dpt = request.json["dpt"]
        inputs = ASDAproxEstandar.MexicoObtenerSupuestos(tidt, ild, dpt, T)
    else:
        dtvml = request.json["dtvml"]
        dtifml = request.json["dtifml"]
        dtivme = request.json["dtivme"]
        dtfml = request.json["dtfml"]
        tidme = request.json["tidme"]
        tidml = request.json["tidml"]
        inputs = ASDAproxEstandar.InternacionalObtenerSupuestos(
            dtvml, dtifml, dtivme, dtfml, tidme, tidml, T
        )

    return jsonify({"inputs": inputs}), 201


@app.route("/api/v1.0/ASD/standarapprox/results", methods=["POST"])
def asd_approxstandard_results():

    catalog = request.json["catalog"]
    r = request.json["r"]
    g = request.json["g"]

    rstd = request.json["rstd"]
    gstd = request.json["gstd"]

    fdesv_g = request.json["fdesv_g"]
    fdesv_r = request.json["fdesv_r"]

    ting = request.json["ting"]  # total de ingresos
    teg = request.json["teg"]  # total de egresos
    pint = request.json["pint"]  # pago de intereses
    ild = request.json["ild"][-1]  # ingreso libre deuda

    d = request.json["d"]

    results = ASDAproxEstandar.ComputoASD(
        r, g, rstd, gstd, d, ting, teg, pint, ild, fdesv_g, fdesv_r
    )

    return jsonify(results), 201


@app.route("/api/v1.0/ASD/dynamicdebt", methods=["POST"])
def asd_dynamic_debt():

    shocks = request.json["shocks"]
    dml = request.json["dml"]
    dme = request.json["dme"]
    rme = [x / 100 for x in request.json["rme"]]
    rml = [x / 100 for x in request.json["rml"]]
    tcambio = request.json["tcambio"]
    ingt = request.json["ingt"]
    teg = request.json["teg"]
    pint = request.json["pint"]

    Tini = request.json["Tini"]
    # to include the last year
    Tproy = request.json["Tproy"] + 1

    results = dict()
    results["all_shocks"] = ASDDinamicaDeuda.CalculateTotalShock(
        dml, dme, rml, rme, tcambio, teg, pint, ingt, Tini, Tproy, shocks
    )

    for key, value in shocks.items():
        single_shock = {}
        single_shock[key] = value
        # json_single_shock = json.dumps(data)
        results[key] = ASDDinamicaDeuda.CalculateTotalShock(
            dml, dme, rml, rme, tcambio, teg, pint, ingt, Tini, Tproy, single_shock
        )
        del results[key]["base_balance_primario_ingreso_total"]
        del results[key]["base_deuda_total_ingreso_total"]
        del results[key]["ingreso_total_desv_std"]
        del results[key]["tipo_cambio_desv_std"]

    return jsonify(results), 201


def params_to_mat(params):
    # 1. Tasa de interés nominal de la deuda en moneda local [rtml]
    # 2. Tasa de interés nominal de la deuda en moneda extranjera [rtme]
    # 3. Tasa de cambio [et]
    # 4. Tasa de crecimiento de los ingresos de libre destinación [gildt]
    # 5. Tasa de crecimiento de los ingresos con destinación específica [gidet]
    # 6. Tasa de crecmiento nominal del gasto primario [ggpt]
    mat = np.array(
        [
            params["rtml"],
            params["rtme"],
            params["et"],
            params["gildt"],
            params["gidet"],
            params["ggpt"],
        ]
    )

    return mat
