import numpy as np

# functions
from . import sofis_utils as sofis_utils

# flask
# import sofis_utils as sofis_utils


def shock_desv_std(proj, proj_data, Tini, Tproy, Tshocki, Tshockf, shock):
    ret_shock = proj
    Tn = Tproy - Tini

    for t in range(1, Tn):
        if t in range(Tshocki - Tini, Tshockf - Tini):
            ret_shock[t] = (
                ret_shock[t - 1]
                * (1 + proj_data["rate_mean"])
                * (1 + shock * proj_data["rate_desv_std"])
            )
        else:
            ret_shock[t] = ret_shock[t - 1] * (1 + proj_data["rate_mean"])

    return ret_shock


def shock_bps(proj, proj_data, Tini, Tproy, Tshocki, Tshockf, shock):
    ret_shock = proj
    Tn = Tproy - Tini
    for t in range(1, Tn):
        if t in range(Tshocki - Tini, Tshockf - Tini):
            ret_shock[t] = ret_shock[t - 1] * (1 + proj_data["rate_mean"]) + (
                shock / 100
            )
        else:
            ret_shock[t] = ret_shock[t - 1] * (1 + proj_data["rate_mean"])

    return ret_shock


def CalculateShock(
    rml, rme, tcambio, ingt, Tini, Tproy, Tshocki, Tshockf, shock, shocktype
):
    T = len(rml)
    Tn = Tproy - Tini
    # shocks a tcambio, ingt  desvstand
    stdrme = np.std(tcambio)
    stdrml = np.std(rml)

    rme_data = sofis_utils.series_data(rme, Tn)
    rml_data = sofis_utils.series_data(rml, Tn)
    tcambio_data = sofis_utils.series_data(tcambio, Tn)
    ingt_data = sofis_utils.series_data(ingt, Tn)

    rme_projection = rme_data["projection"]
    rml_projection = rml_data["projection"]
    tcambio_projection = tcambio_data["projection"]
    ingt_projection = ingt_data["projection"]

    ret_shock = np.zeros(Tproy - Tini - T)
    if shocktype == "tipo_cambio":
        ret_shock = shock_desv_std(
            tcambio_projection, tcambio_data, Tini, Tproy, Tshocki, Tshockf, shock
        )

    if shocktype == "ingreso_total":
        ret_shock = shock_desv_std(
            ingt_projection, ingt_data, Tini, Tproy, Tshocki, Tshockf, shock
        )

    if shocktype == "tasa_int_moneda_local":
        ret_shock = shock_bps(
            rml_projection, rml_data, Tini, Tproy, Tshocki, Tshockf, shock
        )

    if shocktype == "tasa_int_moneda_extranjera":
        ret_shock = shock_bps(
            rme_projection, rme_data, Tini, Tproy, Tshocki, Tshockf, shock
        )

    return ret_shock


def CalculateTotalShock(
    dml, dme, rml, rme, tcambio, teg, pint, ingt, Tini, Tproy, shocks
):

    proj_ingt = CalculateShock(
        rml, rme, tcambio, ingt, Tini, Tproy, 0, 0, 0, "ingreso_total"
    )
    proj_tcambio = CalculateShock(
        rml, rme, tcambio, ingt, Tini, Tproy, 0, 0, 0, "tipo_cambio"
    )
    proj_rml = CalculateShock(
        rml, rme, tcambio, ingt, Tini, Tproy, 0, 0, 0, "tasa_int_moneda_local"
    )
    proj_rme = CalculateShock(
        rml, rme, tcambio, ingt, Tini, Tproy, 0, 0, 0, "tasa_int_moneda_extranjera"
    )

    if "ingreso_total" in shocks:
        shock_ingt = CalculateShock(
            rml,
            rme,
            tcambio,
            ingt,
            Tini,
            Tproy,
            shocks["ingreso_total"]["Ti"],
            shocks["ingreso_total"]["Tf"],
            shocks["ingreso_total"]["shock"],
            "ingreso_total",
        )
    else:
        shock_ingt = proj_ingt

    if "tipo_cambio" in shocks:
        shock_tcambio = CalculateShock(
            rml,
            rme,
            tcambio,
            ingt,
            Tini,
            Tproy,
            shocks["tipo_cambio"]["Ti"],
            shocks["tipo_cambio"]["Tf"],
            shocks["tipo_cambio"]["shock"],
            "tipo_cambio",
        )
    else:
        shock_tcambio = proj_tcambio

    if "tasa_int_moneda_local" in shocks:
        shock_rml = CalculateShock(
            rml,
            rme,
            tcambio,
            ingt,
            Tini,
            Tproy,
            shocks["tasa_int_moneda_local"]["Ti"],
            shocks["tasa_int_moneda_local"]["Tf"],
            shocks["tasa_int_moneda_local"]["shock"],
            "tasa_int_moneda_local",
        )
    else:
        shock_rml = proj_rml

    if "tasa_int_moneda_extranjera" in shocks:
        shock_rme = CalculateShock(
            rml,
            rme,
            tcambio,
            ingt,
            Tini,
            Tproy,
            shocks["tasa_int_moneda_extranjera"]["Ti"],
            shocks["tasa_int_moneda_extranjera"]["Tf"],
            shocks["tasa_int_moneda_extranjera"]["shock"],
            "tasa_int_moneda_extranjera",
        )
    else:
        shock_rme = proj_rme

    proj_teg = sofis_utils.series_data(teg, Tproy - Tini)["projection"]

    # ver que la proj_pint se debe calcular como como projeccion tint*deuda
    proj_pint = sofis_utils.series_data(pint, Tproy - Tini)["projection"]

    proj_dme = sofis_utils.series_data(dme, Tproy - Tini)["projection"]
    proj_dml = sofis_utils.series_data(dml, Tproy - Tini)["projection"]

    # Balance primario / ingreso disponible
    balance_primario = np.zeros(Tproy - Tini)
    for t in range(0, Tproy - Tini):
        balance_primario[t] = shock_ingt[t] - proj_teg[t] + proj_pint[t]

    # Deuda / ingreso disponible
    deuda_total = np.zeros(Tproy - Tini)
    for t in range(0, Tproy - Tini):
        deuda_total[t] = (
            proj_dml[t] * (1 + shock_rml[t])
            + (1 + shock_rme[t]) * shock_tcambio[t] * proj_dme[t]
            - balance_primario[t]
        )

    # Balance primario / ingreso disponible - Base
    balance_primario_base = np.zeros(Tproy - Tini)
    for t in range(0, Tproy - Tini):
        balance_primario_base[t] = proj_ingt[t] - proj_teg[t] + proj_pint[t]

    # Deuda / ingreso disponible - Base
    deuda_total_base = np.zeros(Tproy - Tini)
    for t in range(0, Tproy - Tini):
        deuda_total_base[t] = (
            proj_dml[t] * (1 + proj_rml[t])
            + (1 + proj_rme[t]) * proj_tcambio[t] * proj_dme[t]
            - balance_primario_base[t]
        )

    ret = dict()

    ret["ingreso_total_desv_std"] = sofis_utils.series_data(ingt, Tproy - Tini)[
        "rate_desv_std"
    ]
    ret["tipo_cambio_desv_std"] = sofis_utils.series_data(tcambio, Tproy - Tini)[
        "rate_desv_std"
    ]

    ret["balance_primario_ingreso_total"] = np.divide(
        balance_primario, proj_ingt
    ).tolist()

    ret["balance_primario_ingreso_total"] = np.divide(
        balance_primario, proj_ingt
    ).tolist()
    ret["deuda_total_ingreso_total"] = np.divide(deuda_total, proj_ingt).tolist()
    ret["base_balance_primario_ingreso_total"] = np.divide(
        balance_primario_base, proj_ingt
    ).tolist()
    ret["base_deuda_total_ingreso_total"] = np.divide(
        deuda_total_base, proj_ingt
    ).tolist()

    return ret
