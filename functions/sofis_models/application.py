from . import FanChartsFunction, ASDAproxEstandar, ASDDinamicaDeuda, mfmp
import numpy as np


def info():
    return "SOFIS API - version 1.0"


def totaldebt(request):

    model = request.get("model")
    if model == None:
        model = "var"
    model = model.lower()
    T = request.get("T")
    X = request.get("X")
    alpha = request.get("alpha")
    beta = request.get("beta")

    IDE0 = request.get("IDE0")
    ILD0 = request.get("ILD0")
    GP0 = request.get("GP0")
    D0 = request.get("D0")
    datapoints = request.get("datapoints")

    rtml = request.get("rtml")
    rtme = request.get("rtme")
    et = request.get("et")
    gildt = request.get("gildt")
    gidet = request.get("gidet")
    ggpt = request.get("ggpt")
    # mock for anailises de sensibilidad
    umbrals = request.get("umbrals_for_sensitivity_analysis")

    if request.get("external_series") != None:
        external_series = params_to_mat(request.get("external_series"))
    else:
        external_series = None

    OMT = params_to_mat(request.get("OMT"))
    mu_0 = params_to_mat(request.get("mu_0"))
    mu_1 = params_to_mat(request.get("mu_1"))

    per_sup = 95
    per_inf = 5

    if "per_sup" in request:
        per_sup = request.get("per_sup")
    if "per_inf" in request:
        per_inf = request.get("per_inf")

    totaldebt = FanChartsFunction.DeudaTotal(
        OMT,
        mu_0,
        mu_1,
        T,
        rtml,
        rtme,
        gildt,
        gidet,
        ggpt,
        et,
        external_series,
        D0,
        GP0,
        ILD0,
        IDE0,
        alpha,
        beta,
        X,
        datapoints,
        per_sup,
        per_inf,
        model,
    )

    return {"totaldebt": totaldebt}


def asd_approxstandard_inputs(request):

    catalog = request.get("catalog")
    T = request.get("T")

    if catalog == "M":
        ild = request.get("ild")
        tidt = [x / 100 for x in request.get("tidt")]
        dpt = request.get("dpt")
        inputs = ASDAproxEstandar.MexicoObtenerSupuestos(tidt, ild, dpt, T)
    else:
        dtvml = request.get("dtvml")
        dtifml = request.get("dtifml")
        dtivme = request.get("dtivme")
        dtfml = request.get("dtfml")
        tidme = [x / 100 for x in request.get("tidme")]
        tidml = [x / 100 for x in request.get("tidml")]
        inputs = ASDAproxEstandar.InternacionalObtenerSupuestos(
            dtvml, dtifml, dtivme, dtfml, tidme, tidml, T
        )

    return {"inputs": inputs}


def asd_approxstandard_results(request):

    catalog = request.get("catalog")
    r = request.get("r") / 100
    g = request.get("g") / 100

    rstd = request.get("rstd")
    gstd = request.get("gstd")

    fdesv_g = request.get("fdesv_g")
    fdesv_r = request.get("fdesv_r")

    ting = request.get("ting")  # total de ingresos
    teg = request.get("teg")  # total de egresos
    pint = request.get("pint")  # pago de intereses
    ild = request.get("ild")[-1]  # pago de intereses

    d = request.get("d")

    results = ASDAproxEstandar.ComputoASD(
        r, g, rstd, gstd, d, ting, teg, pint, ild, fdesv_g, fdesv_r
    )

    return results


def asd_dynamic_debt(request):

    shocks = request.get("shocks")
    dml = request.get("dml")
    dme = request.get("dme")
    rme = [x / 100 for x in request.get("rme")]
    rml = [x / 100 for x in request.get("rml")]
    tcambio = request.get("tcambio")
    ingt = request.get("ingt")
    teg = request.get("teg")
    pint = request.get("pint")

    Tini = request.get("Tini")
    # to include the last year
    Tproy = request.get("Tproy") + 1

    results = dict()
    results["all_shocks"] = ASDDinamicaDeuda.CalculateTotalShock(
        dml, dme, rml, rme, tcambio, teg, pint, ingt, Tini, Tproy, shocks
    )

    for key, value in shocks.items():
        single_shock = {}
        single_shock[key] = value
        # json_single_shock = json.dumps(data)
        results[key] = ASDDinamicaDeuda.CalculateTotalShock(
            dml, dme, rml, rme, tcambio, teg, pint, ingt, Tini, Tproy, single_shock
        )
        del results[key]["base_balance_primario_ingreso_total"]
        del results[key]["base_deuda_total_ingreso_total"]
        del results[key]["ingreso_total_desv_std"]
        del results[key]["tipo_cambio_desv_std"]

    return results


def mfmp_approx(request):

    serie = request.get("serie")
    years_to_project = request.get("years_to_project")
    rates = request.get("rates")

    # expected format 1%
    if rates != None:
        rates = [x / 100 for x in rates]

    results = mfmp.aprox(serie, years_to_project, rates)

    if type(results) is list:
        return results
    else:
        return results.tolist()


def params_to_mat(params):
    # 1. Tasa de interés nominal de la deuda en moneda local [rtml]
    # 2. Tasa de interés nominal de la deuda en moneda extranjera [rtme]
    # 3. Tasa de cambio [et]
    # 4. Tasa de crecimiento de los ingresos de libre destinación [gildt]
    # 5. Tasa de crecimiento de los ingresos con destinación específica [gidet]
    # 6. Tasa de crecmiento nominal del gasto primario [ggpt]
    mat = np.array(
        [
            params["rtml"],
            params["rtme"],
            params["et"],
            params["gildt"],
            params["gidet"],
            params["ggpt"],
        ]
    )

    return mat
