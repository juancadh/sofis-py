import logging
import azure.functions as func
import json
from ..sofis_models import application


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info("Python HTTP trigger function processed a request.")

    result = application.asd_approxstandard_results(req.get_json())

    return func.HttpResponse(
        json.dumps(result),
        status_code=200,
        headers={"Content-Type": "apllication/json"},
    )
